/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.sabina.library.entities;

/**
 *
 * @author Sabina
 */


import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
public class UserBook {
    
        
    @Id @GeneratedValue(strategy = GenerationType.AUTO) 
    @Getter @Setter private int id;    
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    @JsonBackReference     
    @Getter @Setter private User user;
    
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bookId")
    @JsonBackReference     
    @Getter @Setter private Book book;
    
    @Getter @Setter private Integer progress;
    
    
}
