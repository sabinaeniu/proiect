/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.sabina.library.entities;

/**
 *
 * @author Sabina
 */
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Entity
public class User {
    
    @Id @GeneratedValue(strategy = GenerationType.AUTO) 
    @Getter @Setter private int id;    
    
    @Getter @Setter private String firstname;
    @Getter @Setter private String lastname;
    @Getter @Setter private String username;
    @Getter @Setter private String password;
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JsonManagedReference
    @Getter @Setter private List<UserBook> userbooks;
    
}
