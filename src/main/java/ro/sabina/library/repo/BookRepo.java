/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.sabina.library.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sabina.library.entities.Book;

/**
 *
 * @author Sabina
 */
public interface BookRepo extends JpaRepository<Book, Integer> {
    
}




