/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.sabina.library.restapi;

/**
 *
 * @author Sabina
 */

import java.util.List;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ro.sabina.library.entities.Book;
import ro.sabina.library.repo.BookRepo;



@RestController
@RequestMapping("/api/books")
public class BookController {
    
    @Autowired
    BookRepo repo;
    
    @RequestMapping("/getall")
    @ResponseBody
    public List<Book> getAll() {
        return repo.findAll();
    }
    
    @RequestMapping("/getbyid")
    @ResponseBody
    public Book getById(Integer id) {
        return repo.findOne(id);
    }
    
    
    
    @GetMapping("/")
    public String getBooks(Model model) {
        model.addAttribute("books", repo.findAll());
        model.addAttribute("book", new Book());
        return "books";
    }
    
    @PostMapping("/")
    @Transactional
    public String addBook(@ModelAttribute Book book) {
        repo.save(book);
        return "redirect:/";
    }

     @GetMapping("/{id}")
    public String selectBook(@PathVariable("id") int id, HttpSession session) {
        session.setAttribute("current", repo.findOne(id));
        return "redirect:/";
    }
    
    @DeleteMapping("/{id}")
    @Transactional
    public String deleteBook(@PathVariable("id") int id) {
        repo.delete(id);
        return "redirect:/";
    }
    
    @PostMapping("/{id}/save")
    @Transactional
    public String saveBook(@PathVariable("id") int id, @ModelAttribute Book book, HttpSession session) {
        System.err.println("why do you call?");
        Book b = repo.findOne(id);
        b.setName(book.getName());
        b.setAuthor(book.getAuthor());
        b.setGenre(book.getGenre());
        b.setYear(book.getYear());
        session.invalidate();
        return "redirect:/";
    }

}
