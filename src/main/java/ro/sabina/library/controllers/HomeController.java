/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.sabina.library.controllers;

/**
 *
 * @author Sabina
 */
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.sabina.library.entities.Book;
import ro.sabina.library.repo.BookRepo;
import ro.sabina.library.repo.UserBookRepo;


@Controller
public class HomeController {
    
    @Autowired BookRepo books;
    @Autowired UserBookRepo userbooks;
    
    @RequestMapping("/")
    public String Index(Map<String, Object> model) {       
        
        model.put("books", books.findAll());
        
        model.put("progress", userbooks.findAll());
        
        
        
        return "index";
    }
    

    
}
